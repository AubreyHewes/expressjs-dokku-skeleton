# ExpressJS Dokku Skeleton

Initially created via [express-generator](http://expressjs.com/starter/generator.html) 

	express expressjs-dokku-skeleton --hbs --css compass --git

 * SCSS via Compass
 * Handlebars template engine
 * Gitignores

## NOTES

 * Default ExpressJS environment is `development` .. this can be overloaded via `NODE_ENV`
 * ENV `development` uses live sass compiling/caching (via compass). This **fatals** within a `dokku` deployment as compass gem can not be installed and is therefore not available.
    * It is currently required to deploy the generated sass->css css file (generated via dev server).
    * It is required to set the dokku environment (NODE_ENV) to a non `development` environment..

@TODO add Grunt css generation task; removing having to access development server to update css..

## Changes from [express-generator](http://expressjs.com/starter/generator.html) 

 * Templates are via `view/templates`
 * SASS is via `view/sass` and generated to `public/stylesheets`

### Reasoning
SASS/SCSS should not be available via `public` the only logical placement is within `view`.. entailing moving templates.

## Dokku SASS (Compass) caveats

Deploying a NodeJS application via dokku does not yet support installing required (global) ruby gems.. 
Therefore it does not support live compass compiling. Which entails having to add the compiled css to the project/deployment.

### This means adding the compiled css to the deployment!

_Preferably I would like to generate the css on deploy else as last resort on first hit.._

## Local development
 
Run the grunt task _local-dev-server_
 
	grunt local-dev-server
