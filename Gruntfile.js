"use strict";

module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-nodemon');
	grunt.loadNpmTasks('grunt-shell');

	// Project configuration.
	grunt.initConfig({
		nodemon: {
			dev: {
				script: 'bin/www',
				options: {
					nodeArgs: ['--debug'],
					ignore: ['node_modules/**', 'public/**/*.js', 'public/**/*.css'],
					ext: 'js,hbs,scss',
					env: {
						NODE_ENV: 'development'
					},
					// omit this property if you aren't serving HTML files and
					// don't want to open a browser tab on start
					callback: function (nodemon) {
						// refreshes browser when server reboots
						nodemon.on('restart', function () {
							// Delay before server listens on port
							setTimeout(function () {
								require('fs').writeFileSync('rebooted.tmp', new Date());
							}, 1000);
						});
					}
				}
			}
		},
		watch: {
			scripts: {
				files: ['./rebooted.tmp'],
				options: {
					livereload: true
				}
			}
		}
	});

	grunt.registerTask('local-dev-server', ['nodemon']);
};